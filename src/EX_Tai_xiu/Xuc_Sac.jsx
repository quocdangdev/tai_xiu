import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TAI, XIU } from './Redux/constant/xucXaConstant'

export class Xuc_Sac extends Component {
  state = {
    active: null,
  }
  handleChangActive = (value) => {
    this.setState({ active: value })
  }
  renderListXucXac = () => {
    return this.props.mangXucXac.map((item, index) => {
      return <img
        src={item.img}
        key={index}
        style={{ width: 100, magin: 10, borderRadius: 8 }} alt=""
      />
    })
  }
  render() {
    let { active } = this.state
    return (
      <div className="d-flex justify-content-between container pt-5">
        <button style={{
          width: 150,
          height: 150,
          fontSize: 40,
          transform: `scale(${active == TAI ? 2 : 1})`
        }} onClick={() => {
          this.handleChangActive(TAI)
        }}
          className="btn btn-danger" >tài</button>
        <div>{this.renderListXucXac()}</div>
        <button style={{
          width: 150,
          height: 150,
          fontSize: 40,
          transform: `scale(${active == XIU ? 2 : 1})`
        }} onClick={() => {
          this.handleChangActive(XIU)
        }} className="btn btn-dark" >xỉu</button>

      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  mangXucXac: state.xucXacReducer.mangXucXac,
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Xuc_Sac)

// phím rcredux