import React, { Component } from 'react'
import bg_game from "../assets/bgGame.png";
import Ket_qua from './Ket_qua';
import Xuc_Sac from './Xuc_Sac';
import "./Game.css"

export default class EX_Tai_xiu extends Component {
    render() {
        return (
            <div
                style={{
                    backgroundImage: `url(${bg_game})`,
                    width: "100vw", height: "100vh"
                }}
                className="bg_game"
            >
                <Xuc_Sac />
                <Ket_qua />
            </div>
        )
    }
}
