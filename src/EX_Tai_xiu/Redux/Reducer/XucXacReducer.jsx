import { PLAY_GAME } from "../constant/xucXaConstant";

let inittialState = {
    mangXucXac: [
        {
            img: './imgXucSac/1.png',
            giaTri: 1,
        },
        {
            img: './imgXucSac/1.png',
            giaTri: 2,
        },
        {
            img: './imgXucSac/1.png',
            giaTri: 3,
        }
    ],
    luaChon: null,
    tongSoLanChoi: 0,
    tongSoLanThang: 0,
}
export const xucXacReducer = (state = inittialState, { type, payload }) => {

    switch (type) {
        case PLAY_GAME: {

            //tạo mã xúc xác mơi
            let newMangXucXac = state.mangXucXac.map((itme) => {
                let random = Math.floor(Math.random() * 6) + 1
                return {
                    img: `./imgXucSac/${random}.png`,
                    giaTri: 1,
                }
            })
            // state.mangXucXac = newMangXucXac; hoặc return lun
            // return { ...state} hoặc
            return { ...state, mangXucXac: newMangXucXac }
        }
        default:
            return state;
    }
}