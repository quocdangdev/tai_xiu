import React, { Component } from 'react'
import { connect } from 'react-redux'
import { PLAY_GAME } from './Redux/constant/xucXaConstant'

class Ket_qua extends Component {
  render() {
    return (
      <div onClick={this.props.handlePlayGame} className='text-center pt-5 display-4'>
        <button className='btn btn-danger'>
          <span className='display-4'>Play game</span>
        </button>
      </div>
    )
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({
        type: PLAY_GAME,
      })
    }
  }
}
export default connect(null, mapDispatchToProps)(Ket_qua)


